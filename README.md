# Cardiologs Recruiting Homework

# Overview

Holter Record Summary

A normal heart beat produces three entities on the ECG — a P wave, a QRS
complex, a T wave.

https://en.wikipedia.org/wiki/Electrocardiography#Theory

Identifying those entities in a signal is called delineation. Here are CSV of
an algorithm output for a 24h ECG:

    https://cardiologs-public.s3.amazonaws.com/python-interview/record.csv

Rows have the following fields:

    - Wave type: P, QRS, T or INV (invalid)
    - Wave onset: Start of the wave in ms
    - Wave offset: End of the wave in ms
    - Optionally, a list of wave tags

This cli tool takes in the CSV algorithm output and gives a general overview of
the results

## How to Run

Firstly, create a virtualenv in order to not muddy up your global python environment
`virtualenv -p $(which python3) ./env`

Next, activate the virtualenv and install requirements
`source ./env/bin/activate && pip install -r requirements.txt`

After requirements have been installed, get acquainted with the script using:
`cli.py --help`

It should give the following output:

```
usage: cli.py [-h] [-r] file

positional arguments:
  file

optional arguments:
  -h, --help    show this help message and exit
  -r, --remote  The file path is a remote file and should be downloaded before parsing
  -o OUTPUT, --output OUTPUT
                A filepath to store the result json
```
