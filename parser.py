from typing import IO, List
from csv import reader
from enums import WaveType


def parse_row(row: List[str]) -> (WaveType, int, int, List[str]):
    """Take in a CSV row and parses it into a tuple of values"""
    return (WaveType[row[0]], int(row[1]), int(row[2]), [] if len(row) < 4 else row[3:])


def parse(file_obj: IO) -> dict:
    """Parser function to read in Cardiologs ECG csv files and return results specified
    by requirements

    Args:
        file: The first parameter.

    Returns:
        A dictionary of all the results from the given file

    Example:
        >>> result = parse(my_file)
        {
            "total": 10,
            "P_premature__count": 2,
            "QRS_premature__count": 1,
            "QRS__count": 4,
            "QRS__min": {"rate": 1000, "timestamp": 235746},
            "QRS__max": {"rate": 3867, "timestamp": 843854},
        }
    """
    results = {
        "total": 0,
        "P_premature__count": 0,
        "QRS_premature__count": 0,
        "QRS__count": 0,
        "QRS__min": {"rate": 0, "timestamp": 0},
        "QRS__max": {"rate": 0, "timestamp": 0},
    }

    start_time = None
    end_time = None
    last_QRS_onset = None
    csv_reader = reader(file_obj)
    for row in csv_reader:
        results["total"] += 1

        # Parse this row and get the values
        wave_type, onset, offset, tags = parse_row(row)

        # Only record the first start time
        if not start_time:
            start_time = onset

        end_time = offset

        if wave_type == WaveType.P and "premature" in tags:
            results["P_premature__count"] += 1

        if wave_type == WaveType.QRS:
            # If this QRS was premature, record it
            if "premature" in tags:
                results["QRS_premature__count"] += 1

            # Add one to our QRS count
            results["QRS__count"] += 1
            if last_QRS_onset is not None:
                rate = onset - last_QRS_onset

                # If we have a new min or this is the first entry, set the new min heartrate
                if (
                    results["QRS__min"]["rate"] > rate
                    or results["QRS__min"]["rate"] == 0
                ):
                    results["QRS__min"] = {"rate": rate, "timestamp": onset}

                # If we have a new max, put a new QRS max heartrate
                if results["QRS__max"]["rate"] < rate:
                    results["QRS__max"] = {"rate": rate, "timestamp": onset}

            last_QRS_onset = onset

    # ms / 1000 = s
    # s / 60 = m
    total_minutes = (end_time - start_time) / 1000 / 60
    results["QRS__avg"] = results["QRS__count"] / total_minutes

    return results