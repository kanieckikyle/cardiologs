import unittest
import os
from unittest.mock import patch

from parser import parse, parse_row
from utils import download_file
from enums import WaveType


class EkgParserTestCase(unittest.TestCase):
    def silent_remove(self, path: str):
        try:
            os.remove(path)
        except OSError:
            pass

    def test_parse_row_no_tags(self):
        row = ["P", "1667", "17778"]

        results = parse_row(row)

        self.assertEqual(results[0], WaveType.P)
        self.assertEqual(results[1], 1667)
        self.assertTrue(isinstance(results[1], int))
        self.assertEqual(results[2], 17778)
        self.assertTrue(isinstance(results[2], int))
        self.assertEqual(len(results[3]), 0)

    def test_parse_row_with_tags(self):
        tags = ["premature", "bad"]
        row = ["QRS", "1111", "6789"] + tags

        results = parse_row(row)

        self.assertEqual(len(results[3]), 2)
        # Make sure all of the tags show up in the parsed results
        self.assertTrue(all([tag in tags for tag in results[3]]))

    def test_download_file(self):
        with patch("utils.requests.get") as mock_get, open(
            "fixtures/data.csv", "rb"
        ) as f:
            mock_get.return_value.content = f.read()

            path = download_file("https://mysupercoolwebsite.com/results.csv")

        self.assertEqual(path, "results.csv")
        self.assertTrue(os.path.exists("results.csv"))

        # I know this is kinda dirty, but it works
        self.addCleanup(self.silent_remove, path)

    def test_parse(self):
        expected = {
            "total": 8,
            "P_premature__count": 2,
            "QRS_premature__count": 1,
            "QRS__count": 2,
            "QRS__min": {"rate": 6, "timestamp": 13},
            "QRS__max": {"rate": 6, "timestamp": 13},
            "QRS__avg": 8000.0,
        }
        with open("fixtures/data.csv", "r") as csv:
            results = parse(csv)

        self.assertDictEqual(results, expected)


if __name__ == "__main__":
    unittest.main()