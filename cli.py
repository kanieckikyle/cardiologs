#!/usr/bin/env python

import json
from argparse import ArgumentParser
from utils import download_file
from parser import parse


def main():
    parser = ArgumentParser()
    parser.add_argument(
        "-r",
        "--remote",
        action="store_true",
        help="The file path is a remote file and should be downloaded before parsing",
    )
    parser.add_argument("-o", "--output", help="A filepath to store the result json")
    parser.add_argument("file")
    args = parser.parse_args()

    path = download_file(args.file) if args.remote else args.file

    with open(path, "r") as csv:
        results = parse(csv)

    user_input = input(
        "Can you remember the date and time this happened? Please enter it below:\n"
    )
    results["datetime"] = user_input.strip()

    output = json.dumps(results, indent=2)
    if args.output:
        with open(args.output, "w") as f:
            f.write(output)
    else:
        # Print the results
        print(output)


if __name__ == "__main__":
    main()