import requests
from enums import WaveType


def download_file(url: str) -> str:
    """Downloads a file from a remote url and saves it in the current directory

    Args:
        url: The url path of the file that will be downloaded

    Returns:
        a string of the filename of the downloaded file
    """
    local_filename = url.split("/")[-1]
    # Stream just in case the file is very large
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, "wb") as f:
            for chunk in r.iter_content(chunk_size=8192):
                f.write(chunk)

    return local_filename
