from enum import Enum


class WaveType(Enum):
    """An Enumeration of WaveType values, so if more are added or the symbol changes, we
    can easily change that across our codebase
    """

    P = "P"
    QRS = "QRS"
    T = "T"
    INV = "INV"